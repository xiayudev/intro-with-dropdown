//Feature
const $btnFeatures = document.getElementById("mobile-feature"),
	$mobileDropFeature = document.getElementById("drop-down-feature-mobile"),
	$arrowFeature = document.querySelector(".arrow-down-mobile-feature");

//Company
const $btnCompany = document.getElementById("mobile-company"),
	$mobileDropCompany = document.getElementById("drop-down-company-mobile"),
	$arrowCompany = document.querySelector(".arrow-down-mobile-company");

//Icon menu - hamburguer
const $iconMenu = document.getElementById("icon-menu"),
	$iconCloseMenu = document.getElementById("icon-close-menu"),
	$menuHam = document.getElementById("menu-ham");

$btnFeatures.addEventListener("click", () => {
	$mobileDropFeature.classList.toggle("hidden");
	$mobileDropFeature.classList.toggle("flex");
	$arrowFeature.classList.toggle("hidden");
});

$btnCompany.addEventListener("click", () => {
	$mobileDropCompany.classList.toggle("hidden");
	$mobileDropCompany.classList.toggle("flex");
	$arrowCompany.classList.toggle("hidden");
});
$iconMenu.addEventListener("click", () => {
	$menuHam.classList.toggle("hidden");
	$menuHam.classList.toggle("flex");
});

$iconCloseMenu.addEventListener("click", () => {
	$menuHam.classList.add("hidden");
	$menuHam.classList.remove("flex");
});
export { };
