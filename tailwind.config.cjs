/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			colors: {
				'almost-white': 'hsl(0, 0%, 98%)',
				'medium-gray': 'hsl(0, 0%, 41%)',
				'almost-black': 'hsl(0, 0%, 8%)',
			},
			fontFamily: {
				'epilogue': ['Epilogue', 'sans-serif'],
			},
			screens: {
				'desktop': '1440px',
			},
			backgroundImage: {
				'img-hero-mobile': "url('/images/image-hero-mobile.png')",
				'img-hero-desktop': "url('/images/image-hero-desktop.png')",
				'icon-todo': "url('/images/icon-todo.svg')",
			}
		},
	},
	plugins: [],
}
