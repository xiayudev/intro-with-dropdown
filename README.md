# Frontend Mentor - Intro section with dropdown navigation solution

This is a solution to the [Intro section with dropdown navigation challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/intro-section-with-dropdown-navigation-ryaPetHE5). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - Intro section with dropdown navigation solution](#frontend-mentor---intro-section-with-dropdown-navigation-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
    - [Useful resources](#useful-resources)
  - [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the relevant dropdown menus on desktop and mobile when interacting with the navigation links
- View the optimal layout for the content depending on their device's screen size
- See hover states for all interactive elements on the page

### Screenshot
- Mobile Design ![Mobile design](public/images/mobile-design.png)
- Desktop Design ![Desktop design](/public/images/desktop-design.png)


### Links

- Solution URL: [Gitlab](https://your-solution-url.com)
- Live Site URL: [Netlify](https://intro-with-dropdown-mentor.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [Astro](https://astro.build/) - JS library
- [Tailwind CSS](https://tailwindcss.com/) - React framework
- Typescript 


### What I learned

I am just getting started with Typescript. This beautiful sintatic language analizer help us to be more accuracy with Javascript. I am improving my skills in Typescript with this kind of projects.

### Continued development

I want to keep going on mastering Typescript.

### Useful resources

- [Typescript](https://www.typescriptlang.org) - This helped me with dropdown mobile menu ham.

## Author

- Website - [xiayudev](https://my-portfolio-v2-1.pages.dev/)
- Frontend Mentor - [@thesunland7](https://www.frontendmentor.io/profile/TheSunLand7)
- Twitter - [@J7Jeo](https://www.twitter.com/J7Jeo)
